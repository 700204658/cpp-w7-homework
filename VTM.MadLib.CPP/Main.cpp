
// Madlib
// Vincent Morrill

#include <iostream>
#include <conio.h>
#include <fstream>
#include <istream>

using namespace std;

void Print(string input) {

	cout << "\n" << input;
}


const int LENGTH = 13;
int main()
{
	
	//init vars
	string words[LENGTH];
	string filepath = "C:\\Users\\User\\AppData\\Temp\\madlibout.txt";
	
	//prompt for values  (10)

	for (int i = 0; i < LENGTH ; i++) {
		cout << "Enter an ";
		if (i == 0 || i == 1 || i == 5 || i == 6 || i == 11) cout << "adjective:\n";
		if ( i == 2 || i == 4) cout << "first name:\n";
		if ( i == 7 || i == 12) cout << "plural noun:\n";
		if ( i == 3) cout << "past tense verb:\n";
		if ( i == 8 ) cout << "large animal:\n";
		if ( i == 9 ) cout << "small animal:\n";
		if ( i == 10) cout << "girl's name:\n";
		cin >> words[i];
	}
	
	//assemble string
	string madlib = "I the " +
		words[0] + " and " +
		words[1] + " " +
		words[2] + " has " +
		words[3] + " " +
		words[4] + "'s " +
		words[5] + " sister and plans to steal her " +
		words[6] + " " +
		words[7] + "!\n What are a " +
		words[8] + " and backpacking " +
		words[9] + " to do? Before you can help " +
		words[10] + " you'll have to collect the " +
		words[11] + " " + words[12] + "\n";

	//print out to console
	Print(madlib);

	char in = 'n';
	//ask if they want it saved to a text file
	cout << "Would you like to save this to a file?(y/n)\n";
	cin >> in;
	
	if (in == 'y' || in == 'Y') {

		ofstream fout(filepath);
		//fout.open(filepath);
		if (fout) {
			fout << madlib;
			fout.close();
		}
		cout << "\nFile has been saved.";
	}
	else {
		return 0;
	}

	return 0;
}
